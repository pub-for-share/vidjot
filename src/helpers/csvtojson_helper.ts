import { uploadEventEmitter } from '../routes/uploads';
import CSVToJSON from 'csvtojson';
import { EventEmitter } from 'events';

export const csvtojsonEventEmitter = new EventEmitter();

const masterObjKey: string = "machineID";

const arrayObjB = [{ machineID: "A01", data1: 102, data2: 103, data3: 104 }, {
    machineID: "A02", data1: 202,
    data2: 203, data3: 204
}, { machineID: "A03", data1: 302, data2: 303, data3: 304 },
{ machineID: "A01", data1: 112, data2: 113, data3: 114 }, { machineID: "A03", data1: 312, data2: 313, data3: 314 },]

const objTransform = (arrayOfObjects: any) => {

    const returnObj = arrayOfObjects.reduce((acc: any, cur: any, curIdx: any) => {
        let returnObj = {};
        let isIdExist: boolean = false;
        let matchedItemNumber: string = '0';
        if (masterObjKey in cur) {
            const { [masterObjKey]:machineID, ...remainProp } = cur;
            if (Object.entries(acc).length === 0) { //first item in array                
                returnObj = {
                    ...acc,
                    [curIdx]: {
                        [masterObjKey]: cur[masterObjKey],
                        data: [remainProp]
                    }
                }
            } else {
                for (const itemNum in acc) {
                    if (acc[itemNum][masterObjKey] === cur[masterObjKey]) {
                        isIdExist = true;
                        matchedItemNumber = itemNum;
                        break;
                    } else {
                        isIdExist = false;
                    }
                }
                if (isIdExist === false) {
                    returnObj = {
                        ...acc,
                        [curIdx]: {
                            [masterObjKey]: cur[masterObjKey],
                            data: [remainProp]
                        }
                    }
                } else {
                    acc[matchedItemNumber].data.push(remainProp)
                    returnObj = {
                        ...acc
                    }
                }
            }
        }
        return returnObj;
    }, {})
    return returnObj;
}



const sampleJsonContent = {"machineID":"A102HF","data":[{"work_number":"1","time":"16:45:53","stage_no":"14","mount_x_coordinate":"280267","pkg_x_correction_amount":"234","coating_insp_ctr_gravity_x":"144","blank_x_slide_amnt":"-40","mount_y_coord":"2390","pkg_y_correction_amount":"89","coating_insp_ctr_gravity_y":"153","blank_y_slide_amnt":"45","mount_0_coord":"317","pkg_0_correction_amount":"-24","coating_insp_ctr_gravity_0":"-8","blank_0_dev_amnt":"-52"}]}

const csvtojsonStart = () => {
    uploadEventEmitter.on('File-Ready', async (filepath) => {
        try {
            // :TODO Convert to array for validation
            // const csvRow = await CSVToJSON({noheader:true,output:"csv"}).fromFile(filepath);
            // console.log(csvRow);
            const jsonContent = await CSVToJSON({ flatKeys: true }).fromFile(filepath);
            const transformedObj = objTransform(jsonContent);
            //console.log(JSON.stringify(transformedObj[0]));            
            csvtojsonEventEmitter.emit('json-ready', sampleJsonContent);
        } catch (e) {
            console.log(e);
        }
    })
}

export default csvtojsonStart;