import { Request, Response, RequestHandler } from 'express';


export const ensureAuthenticated: RequestHandler = (req: Request, res: Response, cb: Function) => {
    if (req.isAuthenticated()) {
        return cb();
    }
    req.flash('error_msg', 'Not Authorized');
    res.redirect('/users/login');
}
