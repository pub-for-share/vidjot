import express, { Express, Request, Response, NextFunction } from 'express';
import exphbs from 'express-handlebars';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import methodOverride from 'method-override';
import flash from 'connect-flash';
import session from 'express-session';
import ideas from './routes/ideas';
import users from './routes/users';
import uploads from './routes/uploads';
import path from 'path';
import passport from 'passport';
import passportInit from './config/passport';
import mongoURI from './config/database';
import csvtojsonStart from './helpers/csvtojson_helper';
import mongoHelperInit from './helpers/mongo_helper';



const port: any = process.env.PORT || 5000;
console.log(`port ${port}`);
console.log(process.env.NODE_ENV);
console.log('mongoURI: ', mongoURI);

const app: Express = express();




//Handlebars middleware
app.engine('handlebars', exphbs({
    defaultLayout: 'main'
}));

app.set('view engine', 'handlebars');
app.use(session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
passportInit();

//Body parser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//static folder
app.use(express.static(path.join(__dirname, '../src', 'public'))); //need to add ../src, otherwise, the path will refer to dist


app.use(methodOverride('_method'));
app.use(flash());



//Global var
app.use((req: Request, res: Response, next: NextFunction) => {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;
    next();
})

app.get('/', (req: Request, res: Response) => {
    const title: string = 'IQL';
    res.render('home', {
        title: title
    });
});

app.get('/about', (_req: Request, res: Response) => {
    res.render('about');
});


app.use('/ideas', ideas);
app.use('/users', users);
app.use('/uploads', uploads);


const main = async () => {
    csvtojsonStart();
    mongoHelperInit();
    try {
        await mongoose.connect('mongodb://localhost/vidjot-dev', { useNewUrlParser: true, useUnifiedTopology: true });
        console.log('MongoDB Connected');
    } catch (e) {
        console.log(e);
    }
    app.listen(port, () => {
        console.log("Start")
    });
};

main();



