import passport from 'passport';
import { Strategy } from 'passport-local';
import * as bcryptjs from 'bcryptjs';
import User from '../models/Users';
import * as passportJWT from 'passport-jwt';
import dotenv from 'dotenv';
dotenv.config();
// Configure the local strategy for use by Passport.
//
// The local strategy require a `verify` function which receives the credentials
// (`username` and `password`) submitted by the user.  The function must verify
// that the password is correct and then invoke `cb` with a user object, which
// will be set at `req.user` in route handlers after authentication.

const passportInit = () => {
    passport.use(new Strategy(
        {
            usernameField: 'email',
            passwordField: 'password',
        },
        async (email: string, password: string, cb: Function) => {
            try {
                let user = await User.findOne({ email: email });
                if (!user) return cb(null, false, { message: 'No User Found' });
                let passwordCorrect = await bcryptjs.compare(password, user.password);
                return passwordCorrect ? cb(null, user) : cb(null, false, { message: 'Password incorrect' });
            } catch (e) {
                return cb(e);
            }
        }
    ));
    //curl -X GET localhost:5000/ideas/test -H "Authorization: Bearer ey....eyJ....3Xr" -v
    passport.use('token-header', new passportJWT.Strategy({
        jwtFromRequest: passportJWT.ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: process.env.JWT_SECRET
    },
        async (jwtPayload, cb) => {
            try {                
                let user = await User.findOne({ _id: jwtPayload.id });
                if (!user) return cb(null, false, { message: 'No User Found' });                
                return cb(null, user);
            } catch (e) {
                return cb(e);
            }
        }
    ));

    //curl -X GET http://localhost:5000/ideas/test -H "Content-Type: application/json" -d '{"token":"ey...ey...3X"}' -v
    passport.use('token-body', new passportJWT.Strategy({
        jwtFromRequest: passportJWT.ExtractJwt.fromBodyField('token'),
        secretOrKey: process.env.JWT_SECRET
    },
        async (jwtPayload, cb) => {
            try {                
                let user = await User.findOne({ _id: jwtPayload.id });
                if (!user) return cb(null, false, { message: 'No User Found' });                
                return cb(null, user);
            } catch (e) {
                return cb(e);
            }
        }
    ));


    passport.serializeUser((document: any, cb: Function) => {
        cb(null, document._id);
    });

    passport.deserializeUser((documentId, cb: Function) => {
        User.findById(documentId, (err, user) => {
            cb(err, user);
        });
    });


}
export default passportInit;


