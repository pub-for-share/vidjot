let mongoURI:String;

if(process.env.NODE_ENV==='production'){
    mongoURI = 'Web mongo';
}else{
    mongoURI = 'local mongo';
}

export default mongoURI;