
import express, { Router, Request, Response } from 'express';
const router: Router = express.Router();
import dotenv from 'dotenv';
dotenv.config();
import multer from 'multer';
import path from 'path';
import {EventEmitter} from 'events';

export const uploadEventEmitter=new EventEmitter();

const uploadFileStorage = multer.diskStorage({
    destination: './uploads',
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
})

const upload = multer({
    storage: uploadFileStorage,
    limits: { fileSize: 1000000 },
    fileFilter: (req, file, cb) => {
        checkFileType(file, cb);
    }
}).single('mycsv');

const checkFileType = (file:any, cb:any) => {
    const fileTypes = /\.csv|\.dat/;
    const fileExt = fileTypes.test(path.extname(file.originalname).toLowerCase());    
    if(fileExt){
        return cb(null,true)
    }else{
        cb('Error: CSV Only!');
    }
}

// curl -X POST -H "Content-Type: multipart/form-data" -F "mycsv=@mountsmall.csv" http://localhost:5000/uploads
router.post('/', (req: Request, res: Response) => {
    console.log('upload triggered\n');
    upload(req, res, (err: any) => {
        if (err) {
            console.log(err);
        } else {            
            uploadEventEmitter.emit('File-Ready',req.file.path);
            res.send('test\n');
        }
    })
})

export default router