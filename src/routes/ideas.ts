import express, { Router, Request, Response } from 'express';
const router: Router = express.Router();
import ideas from '../models/Ideas';
import { ensureAuthenticated } from '../helpers/auth';
import passport from 'passport';


//Original user the successRedirect: '/ideas' and session : true method, which can pass req to this router
// router.get('/', async (req: Request, res: Response) => {
//     console.log(req.user)
//     try {
//         if (req.user) {
//             let resultIdeas = await ideas.find({ user: req.user._id }).sort({ date: 'desc' });
//             res.render('ideas/index', {
//                 ideas: resultIdeas.map(idea => idea.toJSON())
//                 //new version handlebar issue: The models of Mongoose are classes and the properties are not "own properties" of the parent object, so need to conver toJSON first
//                 //https://github.com/handlebars-lang/handlebars.js/issues/1642
//             });
//         }else{
//             console.log('no req.user')
//         }
//     } catch (e) {
//         console.log(e);
//     }
// });

router.get('/user/:user_id', async (req: Request, res: Response) => {
    console.log(req.params.user_id)
    try {
        if (req.params.user_id) {
            let resultIdeas = await ideas.find({ user: req.params.user_id }).sort({ date: 'desc' });
            res.render('ideas/index', {
                ideas: resultIdeas.map(idea => idea.toJSON()),
                user_id: req.params.user_id
                //new version handlebar issue: The models of Mongoose are classes and the properties are not "own properties" of the parent object, so need to conver toJSON first
                //https://github.com/handlebars-lang/handlebars.js/issues/1642
            });
        } else {
            console.log('no req.user')
        }
    } catch (e) {
        console.log(e);
    }
});

router.get('/test', passport.authenticate('token-body', { session: false }), (_req: Request, res: Response) => {
    res.send('test ok\n');
});


// router.get('/add', passport.authenticate('jwt', { session: false }), (_req: Request, res: Response) => {
//     res.render('ideas/add');
// });

router.get('/add/:_id', (req: Request, res: Response) => {
    res.render('ideas/add', {
        id: req.params._id
    });
});

router.post('/edit/:_id', async (req: Request, res: Response) => {
    console.log(req.body);
    try {
        let idea = await ideas.findOne({ _id: req.params._id });
        if (idea !== null) {            
            if (req.body.user_id) {
                if (idea.user != req.body.user_id) {
                    req.flash('error_msg', 'Not Authorized');
                    res.redirect('/ideas/user/'+req.body.user_id);

                } else {
                    res.render('ideas/edit', {
                        title: idea?.title, //?.optional chaining
                        details: idea?.details,
                        id: idea?._id,
                        user_id:req.body.user_id
                    });
                }
            }
        } else {
            console.log('record not found');
        }
    } catch (e) {
        console.log(e);
    }
});

// router.post('/', ensureAuthenticated, async (req: Request, res: Response) => {
//     let errors: Array<{ text: string }> = [];
//     if (!req.body.title) {
//         errors.push({ text: 'Please add a title' });
//     }
//     if (!req.body.details) {
//         errors.push({ text: 'Please add a detail' });
//     }
//     if (errors.length > 0) {
//         res.render('ideas/add', {
//             errors: errors,
//             title: req.body.title,
//             details: req.body.details
//         });
//     } else {
//         if (req.user) {
//             const newUser = {
//                 title: req.body.title,
//                 details: req.body.details,
//                 // user: req.user._id
//                 user: req.user._id
//             }
//             let idea = new ideas(newUser);
//             try {
//                 await idea.save();
//                 req.flash('success_msg', 'Video idea updated');
//                 res.redirect('/ideas');
//             } catch (e) {
//                 console.log(e)
//             }
//         }
//     }
// });

router.post('/', async (req: Request, res: Response) => {
    let errors: Array<{ text: string }> = [];
    if (!req.body.title) {
        errors.push({ text: 'Please add a title' });
    }
    if (!req.body.details) {
        errors.push({ text: 'Please add a detail' });
    }
    if (errors.length > 0) {
        res.render('ideas/add', {
            errors: errors,
            title: req.body.title,
            details: req.body.details
        });
    } else {
        console.log(req.body)
        if (req.body.id) {            
            const newUser = {
                title: req.body.title,
                details: req.body.details,
                // user: req.user._id
                user: req.body.id
            }
            let idea = new ideas(newUser);
            try {
                await idea.save();
                req.flash('success_msg', 'Video idea updated');
                res.redirect('/ideas/user/'+req.body.id);
            } catch (e) {
                console.log(e)
            }
        }
    }
});




router.put('/:_id', async (req: Request, res: Response) => {
    try {
        let resultIdea = await ideas.findOne({ _id: req.params._id });
        if (resultIdea !== null) {
            resultIdea.title = req.body.title;
            resultIdea.details = req.body.details;
            try {
                await resultIdea.save();
                req.flash('success_msg', 'Machine data updated');
                res.redirect('/ideas/user/'+req.body.user_id);
            } catch (e) {
                console.log(e);
            }
        } else {
            console.log('Not Found');
        }
    } catch (e) {
        console.log(e);
    }
});

router.delete('/:id', async (req: Request, res: Response) => {
    try {
        await ideas.deleteOne({ _id: req.params.id })
        req.flash('success_msg', 'Video idea removed');
        res.redirect('/ideas/user/'+req.body.user_id);
    } catch (e) {
        console.log(e);
    }
});

export default router;