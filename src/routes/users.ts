
import express, { Router, Request, Response } from 'express';
const router: Router = express.Router();
import Users from '../models/Users';
import bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import passport from 'passport';
import dotenv from 'dotenv';
dotenv.config();

router.get('/login', (req: Request, res: Response) => {
    res.render('users/login');
});

router.get('/register', (req: Request, res: Response) => {
    res.render('users/register');
});

router.post('/register', async (req: Request, res: Response) => {
    //let errors: Array<{ text: string }> = [];
    let errors: { text: string }[] = [];
    if (req.body.password !== req.body.password2) {
        errors.push({ text: 'Passwords do not match' });
    }
    if (req.body.password.length < 4) {
        errors.push({ text: "Pwd need >4" });
    }
    if (errors.length > 0) {
        res.render('users/register', {
            errors: errors,
            name: req.body.name,
            email: req.body.email,
            password: req.body.password,
            password2: req.body.password2
        })
    } else {
        const newUser = new Users({
            name: req.body.name,
            email: req.body.email,
            password: req.body.password
        });
        try {
            let user = await Users.findOne({ email: req.body.email });
            if (user) {
                console.log('user exist');
                req.flash('error_msg', 'email already reg');
                res.redirect('/users/register');
            } else {
                bcrypt.genSalt(10, (err, salt) => {
                    bcrypt.hash(newUser.password, salt, async (err, hash) => {
                        if (err) throw err;
                        newUser.password = hash;
                        try {
                            await newUser.save();
                            req.flash('success_msg', 'Reg success');
                            res.redirect('/users/login');
                        } catch (e) {
                            console.log(e);
                        }
                    })
                })
            }
        } catch (e) {
            console.log(e);
        }
    }
});

router.post('/login',
    passport.authenticate('local',
        {
            //successRedirect: '/ideas',
            failureRedirect: 'login',
            failureFlash: true,
            session: false
        }),
    (req: Request, res: Response) => {
        const payload = { id: req.user?._id };
        let token: string;
        if (process.env.JWT_SECRET) {
            token = jwt.sign(payload, process.env.JWT_SECRET);
        } else {
            token = jwt.sign(payload, 'error');
        }
        //res.json({ message: "ok", token: token });
        res.redirect('/ideas/user/'+req.user?._id);
    }
);

router.get('/logout', (req: Request, res: Response) => {
    req.logout();
    req.flash('success_msg', 'You are logged out');
    res.redirect('/users/login');
}
);


export default router;