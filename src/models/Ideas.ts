import mogoose, { Schema, Document } from 'mongoose';

interface IIdea extends Document {
    title: string,
    details: string,
    user: string,
    date: Date
}

const IdeaSchema: Schema = new Schema<IIdea>({
    title: {
        type: String,
        required: true
    },
    details: {
        type: String,
        required: true
    },
    user: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    }
});


export default mogoose.model<IIdea>('idea', IdeaSchema);

