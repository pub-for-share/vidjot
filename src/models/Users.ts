import mogoose, { Schema, Document } from 'mongoose';

export interface IUser extends Document {  
    _id: string,  
    name: string,
    email: string,
    password: string,
    date: Date
}

const UserSchema: Schema = new Schema<IUser>({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    }
});

export default mogoose.model<IUser>('user', UserSchema);

