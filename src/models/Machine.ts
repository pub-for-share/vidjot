import mongoose, { Schema, Document } from 'mongoose';

export interface IIndivMachine extends Document {
    work_number: string,
    time: string,
    stage_no: string,
    mount_x_coordinate: string,
    pkg_x_correction_amount: string,
    coating_insp_ctr_gravity_x: string,
    blank_x_slide_amnt: string,
    mount_y_coord: string,
    pkg_y_correction_amount: string,
    coating_insp_ctr_gravity_y: string,
    blank_y_slide_amnt: string,
    mount_0_coord: string,
    pkg_0_correction_amount: string,
    coating_insp_ctr_gravity_0: string,
    blank_0_dev_amnt: string,
}

export interface IMachine extends Document {
    machineID: string,
    data: Array<IIndivMachine>
}

const IndivMachineData: Schema = new Schema<IIndivMachine>(
    {
        work_number: {
            type: String,
            required: true
        },
        time: {
            type: String,
            required: true
        },
        stage_no: {
            type: String,
            required: true
        },
        mount_x_coordinate: {
            type: String,
            required: true
        },
        pkg_x_correction_amount: {
            type: String,
            required: true
        },
        coating_insp_ctr_gravity_x: {
            type: String,
            required: true
        },
        blank_x_slide_amnt: {
            type: String,
            required: true
        },
        mount_y_coord: {
            type: String,
            required: true
        },
        pkg_y_correction_amount: {
            type: String,
            required: true
        },
        coating_insp_ctr_gravity_y: {
            type: String,
            required: true
        },
        blank_y_slide_amnt: {
            type: String,
            required: true
        },
        mount_0_coord: {
            type: String,
            required: true
        },
        pkg_0_correction_amount: {
            type: String,
            required: true
        },
        coating_insp_ctr_gravity_0: {
            type: String,
            required: true
        },
        blank_0_dev_amnt: {
            type: String,
            required: true
        }
    }, {
    timestamps: true
    }
)

const MachineSchema: Schema = new Schema<IMachine>({
    machineID: {
        type: String,
        required: true,
        trim:true
    },
    data: [IndivMachineData]
});

//export default mogoose.model<IMachine>('machines', MachineSchema);
export default mongoose.model<IMachine>('Machines', MachineSchema,'machines');

